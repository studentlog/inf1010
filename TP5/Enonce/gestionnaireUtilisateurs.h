/********************************************
* Titre: Travail pratique #5 - gestionnaireUtilisateurs.h
* Date: 4 novembre 2018
* Auteur: Ryan Hardie
*******************************************/

#pragma once

#include "utilisateur.h"
#include "gestionnaireGenerique.h"
#include <map>
#include <vector>
#include "foncteur.h"

class GestionnaireUtilisateurs:public GestionnaireGenerique<map<Utilisateur*, double>, Utilisateur*, AjouterUtilisateur, pair<Utilisateur*, double>> 
{
public:
	/*Utilisateur* getUtilisateurParIndex(int i) const;
	vector<Utilisateur*> getUtilisateurs() const;
	int getNombreUtilisateurs() const;
	int getIndexDe(Utilisateur* utilisateur) const;
	GestionnaireUtilisateurs& ajouterUtilisateur(Utilisateur* utilisateur);*/
	
	vector<double> getComptes() const;
	//Retourne un vector contenant les comptes des utilisateurs

	bool estExistant(Utilisateur* utilisateur) const;
	//Retourne true si	l�utilisateur existe, sinon faux. (Cette m�thode vous est d�j� fournie)
		
	void mettreAJourComptes(Utilisateur* payePar, double montant);
	// Met � jour les comptes avec le montant pass� en param�tre. (R�f�rez - vous � la m�thode
	//ajouterDepense de la classe Groupe)
	

	
	/*************************************************************************************/
	
	/* Le double de la pair <Utilisateur*, double> correspond au compte. Le compte
	sera d�sormais directement associ� � l'Utilisateur � qui il appartient. Il
	faudra donc supprimer le vector<double> comptes_ de la classe Groupe.  */

	pair<Utilisateur*, double>& getMax() const;
	//Retourne l��l�ment de la map ayant la valeur de compte la plus �lev�e

	pair<Utilisateur*, double>& getMin() const;
	//Retourne l��l�ment de la map ayant la valeur de compte la plus faible

	/**************************************************************************************/


	

	Utilisateur * getUtilisateurSuivant(Utilisateur* utilisateur, double montant) const;
	// Retourne l�utilisateur suivant l�utilisateur pass� par param�tre
	//Cette m�thode doit utiliser la fonction find_if de la STL avec un bind et un placeholder

	vector<pair<Utilisateur*, double>> getUtilisateursEntre(double borneInf, double borneSup) const;
	// Retourne un vector contenant les utilisateur compris entre la borne inf�rieure et la borne sup�rieure.
	//Dans cette m�thode, il faut utiliser le FoncteurIntervalle que vous allez impl�menter, la fonction copy_if de la STL et
	//un back_inserter.

	GestionnaireUtilisateurs & setCompte(pair<Utilisateur*, double> p);
	//Met � jour la pair<Utilisateur*, double> dans la map<Utilisateur*, double>.

//private:
	//vector<Utilisateur*> utilisateurs_;
};