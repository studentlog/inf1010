/********************************************
* Titre: Travail pratique #5 - foncteur.h
* Date: 4 novembre 2018
* Auteur: Ryan Hardie
* Mise a jour: 9 novembre 2018 
* Modifie par: Louis Couillard
*******************************************/


#pragma once
#include <map>
#include "utilisateur.h"
#include "utilisateurPremium.h"
#include "utilisateurRegulier.h"

class AjouterDepense{
public:
	AjouterDepense(vector<Depense*>& unConteneur) :conteneur_(unConteneur) {};
	vector<Depense*>& operator()(Depense* uneDepense)
	{
		conteneur_.push_back(uneDepense);
		return conteneur_;
	}

private:
	vector<Depense*> conteneur_;
};



//TODO : terminer operator()
//Reste a mettre les conditions d'ajout pour ajouter un Utilisateur dans operator()
class AjouterUtilisateur {
public:
	AjouterUtilisateur(map<Utilisateur*, double>& unConteneur): conteneur_(unConteneur) {};
	map<Utilisateur*, double>& operator()(Utilisateur* unUtilisateur)
	{
		UtilisateurRegulier* utilisateurRegulier = dynamic_cast<UtilisateurRegulier*>(unUtilisateur);
		UtilisateurPremium* utilisateurPremium = dynamic_cast<UtilisateurPremium*>(unUtilisateur);

		if (utilisateurRegulier != nullptr) {
			if (!utilisateurRegulier->getPossedeGroupe()) {
				utilisateurRegulier->setPossedeGroupe(true);
			}
			else {
				cout << "Erreur : L'utilisateur " << unUtilisateur->getNom() << " n'est pas un utilisateur premium et est deja dans un groupe." << endl;
				return conteneur_;
			}
		}
		else {
			if (utilisateurPremium != nullptr && utilisateurPremium->getJoursRestants() <= 0) {
				cout << "Erreur : L'utilisateur " << unUtilisateur->getNom() << " doit renouveler son abonnement premium" << endl;
				return conteneur_;
			}
		}

		conteneur_.insert(make_pair(unUtilisateur, 0.0));
		return conteneur_;
	}

private:
	map<Utilisateur*, double> conteneur_;
};



class FoncteurIntervalle {
public:
	FoncteurIntervalle(double inf, double sup)
		:borneInf_(inf), borneSup_(sup) {};

	bool operator()(pair<Utilisateur*, double> unePair) {
		if (unePair.second >= borneInf_
			&& unePair.second <= borneSup_)
			return true;
		else
			return false;
	};

private:
	double borneInf_;
	double borneSup_;
};
/*
Foncteur FoncteurIntervalle

M�thode :
Constructeur
bool operator()();

Attribut :
double borneInf_, borneSup_;
*/
