/********************************************
* Titre: Travail pratique #5 - gestionnaireUtilisateurs.cpp
* Date: 4 novembre 2018
* Auteur: Ryan Hardie
*******************************************/

#include "gestionnaireUtilisateurs.h"
#include "gestionnaireGenerique.h"
#include "utilisateurPremium.h"
#include "utilisateurRegulier.h"
#include "Foncteur.h"
#include <functional>
#include <numeric>
#include <utility>
#include <algorithm>

vector<double> GestionnaireUtilisateurs::getComptes() const
{
	vector<double> vecteurDeComptes;
	back_insert_iterator<vector<double>>  it(vecteurDeComptes);
	for (auto it = conteneur_.begin(); it != conteneur_.end(); it++)
	{
		vecteurDeComptes.push_back(it->second);
	}
	return vecteurDeComptes;
}



bool GestionnaireUtilisateurs :: estExistant(Utilisateur* utilisateur) const
{
	bool estExistant = false;
	for (auto it = conteneur_.begin(); it != conteneur_.end(); it++)
	{
		if (it->first == utilisateur)
		{
			estExistant = true;
		}
		return estExistant;
	}
}



void GestionnaireUtilisateurs::mettreAJourComptes(Utilisateur* payePar, double montant)
{
	
	if (estExistant(payePar) == true)
	{
		double montantReparti = montant / conteneur_.size;
		for (auto it = conteneur_.begin(); it != conteneur_.end(); it++)
		{
			if (it->first != payePar)
			{
				it->second += montantReparti;
			}			
		}
	}

}



pair<Utilisateur*, double>& GestionnaireUtilisateurs::getMax() const
{
	if (!conteneur_.empty())
	{
		auto UtilisateurCompteMax = max_element(conteneur_.begin(), conteneur_.end(),
			[](pair<Utilisateur*, double>&  compte1, pair<Utilisateur*, double>& compte2)
		{
			return (compte1.second < compte2.second);
		});
		return *UtilisateurCompteMax;
	}
	else return nullptr;
}
//Retourne l��l�ment de la map ayant la valeur de compte la plus �lev�e



pair<Utilisateur*, double>& GestionnaireUtilisateurs::getMin() const
{

}
//Retourne l��l�ment de la map ayant la valeur de compte la plus faible























/**********************************************************************************
Utilisateur* GestionnaireUtilisateurs::getUtilisateurParIndex(int i) const
{
	return utilisateurs_[i];
}

vector<Utilisateur*> GestionnaireUtilisateurs::getUtilisateurs() const
{
	return utilisateurs_;
}

int GestionnaireUtilisateurs::getNombreUtilisateurs() const
{
	return utilisateurs_.size();
}

int GestionnaireUtilisateurs::getIndexDe(Utilisateur * utilisateur) const
{
	int index = -1;
	for (int i = 0; i < utilisateurs_.size(); i++) {
		if (utilisateurs_[i] == utilisateur) {
			index = i;
			break;
		}
	}
	return index;
}

GestionnaireUtilisateurs& GestionnaireUtilisateurs::ajouterUtilisateur(Utilisateur * utilisateur)
{
	UtilisateurRegulier* utilisateurRegulier = dynamic_cast<UtilisateurRegulier*>(utilisateur);
	UtilisateurPremium* utilisateurPremium = dynamic_cast<UtilisateurPremium*>(utilisateur);

	if (utilisateurRegulier != nullptr) {
		if (!utilisateurRegulier->getPossedeGroupe()) {
			utilisateurRegulier->setPossedeGroupe(true);
		}
		else {
			cout << "Erreur : L'utilisateur " << utilisateur->getNom() << " n'est pas un utilisateur premium et est deja dans un groupe." << endl;
			return *this;
		}
	}
	else {
		if (utilisateurPremium != nullptr && utilisateurPremium->getJoursRestants() <= 0) {
			cout << "Erreur : L'utilisateur " << utilisateur->getNom() << " doit renouveler son abonnement premium" << endl;
			return *this;
		}
	}

	utilisateurs_.push_back(utilisateur);
	return *this;
}
******************************************************************************************************************************************/