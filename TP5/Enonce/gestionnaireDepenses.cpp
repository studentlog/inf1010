#include "gestionnaireDepenses.h"

#include <algorithm>
#include <functional>

double GestionnaireDepenses::getTotalDepenses() const
{
	double totalDepenses = 0;
	if(!(conteneur_.empty()))
    {
        for (auto it = conteneur_.begin(); it != conteneur_.end(); ++it)
        {
            totalDepenses += (*it)->getMontant();
        }
    }
	return totalDepenses;
}
