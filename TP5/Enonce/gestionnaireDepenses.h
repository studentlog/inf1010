#pragma once

#include "depense.h"
#include "utilisateur.h"
#include "gestionnaireGenerique.h"
#include "foncteur.h"
#include <algorithm>
#include <vector>

class GestionnaireDepenses:public GestionnaireGenerique<vector<Depense*>, Depense*, AjouterDepense, Depense*> {
public:
	//int getIndexDe(Depense * depense) const; //TODO : cette m�thode n'ai pas remplacer par une autre m�thode de la classe g�n�rique.
	double getTotalDepenses() const;
};
