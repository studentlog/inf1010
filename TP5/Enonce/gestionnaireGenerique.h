/********************************************
* Titre: Travail pratique #5 - gestionnaireGenerique.h
* Date: 4 novembre 2018
* Auteur: Ryan Hardie
*******************************************/

/*
Cette classe permet la cr�ation d�un gestionnaire g�n�rique qui permet de manipuler 
plusieurs donn�es d�un m�me typename T (dans notre cas des Utilisateur* ou des Depense*) 
� l�aide d�un conteneur de typename C (qui pourra par exemple soit �tre une 
map<Utilisateur*, double> ou un vector<Depense*>). Le typename D permet de sp�cifier le 
type de retour de la m�thode getElementParIndex() �tant donn� qu�elle peut varier en 
fonction du type de conteneur. Enfin, comme la m�thode d�ajout est diff�rente pour 
chaque conteneur nous allons utiliser des foncteurs d�ajout de typename FoncteurAjouter.
*/

#pragma once

/*#ifndef GESTIONNAIREGENERIQUE_H
#define GESTIONNAIREGENERIQUE_H*/

template< typename C, typename T, typename FoncteurAjouter, typename D >

class GestionnaireGenerique
{
    public:
        C getConteneur() const;
        void ajouter(T t);
        int getNombreElements() const;
        D getElementParIndex(int i) const;

	protected:
        C conteneur_;
};

template< typename C, typename T, typename FoncteurAjouter, typename D >
C GestionnaireGenerique<C, T, FoncteurAjouter, D>::getConteneur() const
{
    return conteneur_;
}



template< typename C, typename T, typename FoncteurAjouter, typename D >
void GestionnaireGenerique<C, T, FoncteurAjouter, D> :: ajouter(T t)
{
	FoncteurAjouter ajouterType(conteneur_);
	ajouterType(t);
}


template< typename C, typename T, typename FoncteurAjouter, typename D >
int GestionnaireGenerique<C, T, FoncteurAjouter, D> :: getNombreElements() const
{
	return conteneur_.size();
}



template< typename C, typename T, typename FoncteurAjouter, typename D >
D GestionnaireGenerique<C, T, FoncteurAjouter, D> :: getElementParIndex(int i) const
{
	D obj = conteneur_[i];
	return obj;
}



//#endif // GESTIONNAIREGENERIQUE_H



